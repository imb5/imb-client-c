﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;

using System.IO;
using System.Net.Sockets;
using System.Net.Security;
using System.Threading;
using System.Security.Cryptography.X509Certificates;
using System.Security.Authentication;

namespace IMB
{
    public class TByteBuffer
    {
        // protobuf wire types
        public const int wtVarInt = 0;                        // int32, int64, uint32, uint64, sint32, sint64, bool, enum
        public const int wt64Bit = 1;                         // double or fixed int64/uint64
        public const int wtLengthDelimited = 2;               // string, bytes, embedded messages, packed repeated fields
        [ObsoleteAttribute("Do not use this const value. Groups are no longer supported.", true)]
        public const int wtStartGroup = 3;                    // deprecated
        [ObsoleteAttribute("Do not use this const value. Groups are no longer supported.", true)]
        public const int wtEndGroup = 4;                      // deprecated
        public const int wt32Bit = 5;                         // float (single) or fixed int32/uint32

        private byte[] fBuffer;
        private int fOffset = 0;
        private int fLimit = 0;

        public TByteBuffer(int aCapacity)
        {
            if (aCapacity > 0)
                fBuffer = new byte[aCapacity];
            fOffset = 0;
            fLimit = fBuffer.Length;
        }

        public TByteBuffer(byte[] aBuffer)
        {
            fBuffer = aBuffer;
            fOffset = 0;
            fLimit = fBuffer.Length;
        }

        public TByteBuffer(params byte[][] aBuffers)
        {
            int capacity = 0;
            foreach (byte[] b in aBuffers)
                capacity += b != null ? b.Length : 0;
            if (capacity > 0)
            {
                fBuffer = new byte[capacity];
                foreach (byte[] b in aBuffers)
                {
                    if (b != null)
                    {
                        System.Buffer.BlockCopy(b, 0, this.fBuffer, this.fLimit, b.Length);
                        this.fLimit += b.Length;
                    }
                }
            }
        }

        public TByteBuffer(List<byte[]> aBuffers)
        {
            int capacity = 0;
            foreach (byte[] b in aBuffers)
                capacity += b.Length;
            if (capacity > 0)
            {
                fBuffer = new byte[capacity];
                foreach (byte[] b in aBuffers)
                {
                    System.Buffer.BlockCopy(b, 0, this.fBuffer, this.fLimit, fBuffer.Length);
                    this.fLimit += b.Length;
                }
            }
        }

        public TByteBuffer Copy()
        {
            var res = new TByteBuffer(this.Limit) { fOffset = this.fOffset };
            System.Buffer.BlockCopy(fBuffer, 0, res.fBuffer, 0, this.Limit);
            return res;
        }

        public byte[] Buffer { get { return fBuffer; } }
        public int Offset { get { return fOffset; } set { fOffset = value; } }
        public int Limit
        {
            get { return fLimit; }
            set
            {
                if (value > fBuffer.Length)
                    Array.Resize<byte>(ref fBuffer, value);
                fLimit = value;
            }
        }


        public int Remaining { get { return fLimit - fOffset; } }

        public byte FirstByte { get { return fBuffer[0]; } }

        public void ShiftLeft(byte aMostRightByte)
        {
            System.Buffer.BlockCopy(fBuffer, 1, fBuffer, 0, fBuffer.Length - 1);
            fBuffer[fBuffer.Length - 1] = aMostRightByte;
        }

        public UInt64 BB_read_uint64() // unsigned varint
        {
            int shiftLeft = 0;
            UInt64 b = 0;
            UInt64 res = 0;
            do
            {
                b = fBuffer[fOffset++];
                res |= (b & 0x7F) << shiftLeft;
                shiftLeft += 7;
            } while (b >= 128);
            return res;
        }

        public Int64 BB_read_int64() // signed varint
        {
            UInt64 ui64 = BB_read_uint64();
            // remove sign bit
            Int64 res = (Int64)(ui64 >> 1);
            // adjust for negative
            if ((ui64 & 1) == 1)
                res = -(res + 1);
            return res;
        }

        public UInt32 BB_read_uint32() // unsigned varint
        {
            int shiftLeft = 0;
            UInt32 b = 0;
            UInt32 res = 0;
            do
            {
                b = fBuffer[fOffset++];
                res |= (b & 0x7F) << shiftLeft;
                shiftLeft += 7;
            } while (b >= 128);
            return res;
        }

        public Int32 BB_read_int32() // signed varint
        {
            UInt32 ui32 = BB_read_uint32();
            // remove sign bit
            Int32 res = (Int32)(ui32 >> 1);
            // adjust for negative
            if ((ui32 & 1) == 1)
                res = -(res + 1);
            return res;
        }

        public UInt16 BB_read_uint16() // fixed 16 bit (cannot be tagged)
        {
            UInt16 u = BitConverter.ToUInt16(fBuffer, fOffset);
            fOffset += 2;
            return u;
        }

        public bool BB_read_bool() // 1 byte varint
        {
            return fBuffer[fOffset++] != 0;
        }

        public double BB_read_double() // 64 bit float
        {
            Double d = BitConverter.ToDouble(fBuffer, fOffset);
            fOffset += 8;
            return d;
        }

        public float BB_read_single() //  32 bit float
        {
            Single s = BitConverter.ToSingle(fBuffer, fOffset);
            fOffset += 4;
            return s;
        }

        public Guid BB_read_guid() // length delimited
        {
            return new Guid(BB_read_bytes());
        }

        public string BB_read_string() // length delimited
        {
            int len = (int)BB_read_uint64();
            if (len > 0)
            {
                string s = Encoding.UTF8.GetString(fBuffer, fOffset, len);
                fOffset += len;
                return s;
            }
            else
                return "";
        }

        public byte[] BB_read_bytes() // length delimited
        {
            int len = (int)BB_read_uint64();
            byte[] r = new byte[len];
            System.Buffer.BlockCopy(fBuffer, fOffset, r, 0, len);
            fOffset += len;
            return r;
        }

        // skip reading
        public void BB_read_skip(int aWiretype)
        {
            switch (aWiretype)
            {
                case wtVarInt:
                    BB_read_uint64();
                    break;
                case wt32Bit:
                    fOffset += 4;
                    break;
                case wt64Bit:
                    fOffset += 4;
                    break;
                case wtLengthDelimited:
                    UInt64 len = BB_read_uint64();
                    fOffset += (int)len;
                    break;
                default:
                    throw new Exception("Unsupported wire type (" + aWiretype.ToString() + ") in TByteBuffer.bb_read_skip");
            }
        }

        public void BB_read_skip_bytes(int aNumberOfBytes) { fOffset += aNumberOfBytes; }

        // join array of byte[] to 1 array of bytes
        public static byte[] BB_join(params byte[][] aBuffers)
        {
            int capacity = 0;
            foreach (byte[] b in aBuffers)
                capacity += (b !=null) ? b.Length : 0;
            byte[] r = new byte[capacity];
            int offset = 0;
            foreach (byte[] b in aBuffers)
            {
                if (b != null)
                {
                    System.Buffer.BlockCopy(b, 0, r, offset, b.Length);
                    offset += b.Length;
                }
            }
            return r;
        }

        // join list of byte[] to 1 array of bytes
        public static byte[] BB_join(List<byte[]> aBuffers)
        {
            int capacity = 0;
            foreach (byte[] b in aBuffers)
                capacity += b.Length;
            byte[] r = new byte[capacity];
            int offset = 0;
            foreach (byte[] b in aBuffers)
            {
                System.Buffer.BlockCopy(b, 0, r, offset, b.Length);
                offset += b.Length;
            }
            return r;
        }

        // field writing
        public static byte[] BB_bool(bool aValue) // unsigned varint
        {
            if (aValue)
                return new byte[] { 1 };
            else
                return new byte[] { 0 };
        }

        public static byte[] BB_uint16(UInt16 aValue) // fixed 16 bit (cannot be tagged)
        {
            return BitConverter.GetBytes(aValue);
        }

        private static int BB_var_int_length(UInt64 aValue)
        {
            // encode in blocks of 7 bits (high order bit of byte is signal that more bytes are to follow
            // encode lower numbers directly for speed
            if (aValue < 128)
                return 1;
            else if (aValue < 128 * 128)
                return 2;
            else if (aValue < 128 * 128 * 128)
                return 3;
            else
            {
                // 4 bytes or more: change to dynamic size detection
                int res = 4;
                aValue >>= 7 * 4;
                while (aValue > 0)
                {
                    aValue >>= 7;
                    res++;
                }
                return res;
            }
        }

        public static byte[] BB_uint64(UInt64 aValue) // unsigned varint
        {
            byte[] ba = new byte[BB_var_int_length(aValue)];
            int offset = 0;
            while (aValue >= 128)
            {
                ba[offset++] = (byte)((aValue & 0x7F) | 0x80); // msb: signal more bytes are to follow
                aValue >>= 7;
            }
            ba[offset++] = (byte)aValue; // aValue<128 (msb already 0)
            return ba;
        }

        public static byte[] BB_uint32(UInt32 aValue) // unsigned varint
        {
            byte[] ba = new byte[BB_var_int_length(aValue)];
            int offset = 0;
            while (aValue >= 128)
            {
                ba[offset++] = (byte)((aValue & 0x7F) | 0x80); // msb: signal more bytes are to follow
                aValue >>= 7;
            }
            ba[offset++] = (byte)aValue; // aValue<128 (msb already 0)
            return ba;
        }

        public static byte[] BB_int64(Int64 aValue)// signed varint
        {
            if (aValue < 0)
                return BB_uint64(((UInt64)(-(aValue + 1)) << 1) | 1);
            else
                return BB_uint64((UInt64)aValue << 1);
        }

        public static byte[] BB_int32(Int32 aValue)// unsigned varint
        {
            if (aValue < 0)
                return BB_uint32(((UInt32)(-(aValue + 1)) << 1) | 1);
            else
                return BB_uint32((UInt32)aValue << 1);
        }

        public static byte[] BB_single(float aValue) // 32 bit
        {
            return BitConverter.GetBytes(aValue);
        }

        public static byte[] BB_double(double aValue) // 64 bit
        {
            return BitConverter.GetBytes(aValue);
        }

        public static byte[] BB_bytes(byte[] aValue) // length delimited
        {
            UInt64 len = (UInt64)aValue.Length;
            return BB_join(BB_uint64(len), aValue);
        }

        public static byte[] BB_string(string aValue) // length delimited
        {
            byte[] ba = Encoding.UTF8.GetBytes(aValue);
            UInt64 len = (UInt64)ba.Length;
            return BB_join(BB_uint64(len), ba);
        }

        public static byte[] BB_guid(Guid aValue) // length delimited
        {
            byte[] ba = aValue.ToByteArray();
            UInt64 len = (UInt64)ba.Length;
            return BB_join(BB_uint64(len), ba);
        }

        // taged field writing
        public static byte[] BB_tag_int32(UInt32 aTag, Int32 aValue)
        {
            return BB_join(BB_uint32(aTag << 3 | wtVarInt), BB_int32(aValue));
        }

        public static byte[] BB_tag_uint32(UInt32 aTag, UInt32 aValue)
        {
            return BB_join(BB_uint32(aTag << 3 | wtVarInt), BB_uint32(aValue));
        }

        public static byte[] BB_tag_int64(UInt32 aTag, Int64 aValue)
        {
            return BB_join(BB_uint32(aTag << 3 | wtVarInt), BB_int64(aValue));
        }

        public static byte[] BB_tag_uint64(UInt32 aTag, UInt64 aValue)
        {
            return BB_join(BB_uint32(aTag << 3 | wtVarInt), BB_uint64(aValue));
        }

        public static byte[] BB_tag_bool(UInt32 aTag, bool aValue)
        {
            return BB_join(BB_uint32(aTag << 3 | wtVarInt), BB_bool(aValue));
        }

        public static byte[] BB_tag_single(UInt32 aTag, float aValue)
        {
            return BB_join(BB_uint32(aTag << 3 | wt32Bit), BB_single(aValue));
        }

        public static byte[] BB_tag_double(UInt32 aTag, double aValue)
        {
            return BB_join(BB_uint32(aTag << 3 | wt64Bit), BB_double(aValue));
        }

        public static byte[] BB_tag_guid(UInt32 aTag, Guid aValue)
        {
            return BB_join(BB_uint32(aTag << 3 | wtLengthDelimited), BB_guid(aValue));
        }

        public static byte[] BB_tag_string(UInt32 aTag, string aValue)
        {
            return BB_join(BB_uint32(aTag << 3 | wtLengthDelimited), BB_string(aValue));
        }

        public static byte[] BB_tag_bytes(UInt32 aTag, byte[] aValue)
        {
            return BB_join(BB_uint32(aTag << 3 | wtLengthDelimited), BB_bytes(aValue));
        }
    }

    class TStreamCacheEntry
    {
        string fStreamName;
        Stream fStream;

        public TStreamCacheEntry(string aStreamName, Stream aStream)
        {
            fStreamName = aStreamName;
            fStream = aStream;
        }

        public string StreamName { get { return fStreamName; } set { fStreamName = value; } }
        public Stream Stream { get { return fStream; } set { fStream = value; } }
    }

    public class TRequest
    {
        // request errors
        public const int reOK = 0;
        public const int reAlreadyExists = 1;
        public const int reTimedOut = 2;
        public const int reNotFound = 3;
        public const int reBusy = -1;

        public TRequest(Guid aRequestID, Action<TEventEntry, TRequest> aOnResponse)
        {
            fRequestID = aRequestID;
            fOnResponse = aOnResponse;
            fRequestComplete = fOnResponse == null ? new ManualResetEvent(false): null;
        }

        ~TRequest()
        {
            Close(null);
        }

        public void Close(TEventEntry aEventEntry)
        {
            RequestError = reTimedOut;
            RequestComplete?.Set();
            RequestComplete?.Close();
            OnResponse?.Invoke(aEventEntry, this);
        }

        public void AddResponse(byte[] aResponse)
        {
            fResponses = new TByteBuffer(fResponses.Buffer, aResponse);
        }

        private Guid fRequestID;
        private TByteBuffer fResponses = new TByteBuffer();
        private Action<TEventEntry, TRequest> fOnResponse;
        private ManualResetEvent fRequestComplete;

        public Guid RequestID { get { return fRequestID; } }
        public TByteBuffer Responses { get { return fResponses; } }
        public int RequestError { get; set; } = reBusy;
        public Action<TEventEntry, TRequest> OnResponse {  get { return fOnResponse; } }
        public ManualResetEvent RequestComplete { get { return fRequestComplete; } }
    }

    // todo: TBatchedBuffer

    public class TEventEntry
    {
        public const int imbMaxStreamBodyBuffer = 8 * 1024;

        // change object actions
        public const int actionNew = 0;
        public const int actionDelete = 1;
        public const int actionChange = 2;
        public const int actionInquire = 3;

        // basic event tags
        public const int icehIntString = 1;                 // <varint>
        public const int icehIntStringPayload = 2;          // <string>
        public const int icehString = 3;                    // <string>
        public const int icehChangeObject = 4;              // <int32: varint>
        public const int icehChangeObjectAction = 5;        // <int32: varint>
        public const int icehChangeObjectAttribute = 6;     // <string>

        public const int icehStreamHeader = 7;              // <string> filename
        public const int icehStreamBody = 8;                // <bytes>
        public const int icehStreamEnd = 9;                 // <bool> true: ok, false: cancel
        public const int icehStreamID = 10;                 // <id: bytes/string>

        public const int icehNoObjectID = 11;               // <id: bytes/string>
        public const int icehObjectID = 12;                 // <id: bytes/string>
        public const int icehNoAttribute = 13;              // <uint32: varint> key of attribute
        public const int icehAttributeBase = 14;            // attributes of object are icehAttributeBase..
        public const int icehAttributeTop = 899;            // todo: may be lower?

        public const int icehWorldCommandBase = 900;        // .. 999

        public const int icehObjectsInquire = icehWorldCommandBase + 0; // wtLengthDelimited: string, filter/query
        public const int icehReturnEventName = icehWorldCommandBase + 1; // wtLengthDelimited: string
        public const int icehTimeStamp = icehWorldCommandBase + 2; // wtInt64: double
        public const int icehTimeStampLower = icehWorldCommandBase + 3; // wtInt64: double
        public const int icehTimeStampUpper = icehWorldCommandBase + 4; // wtInt64: double
        public const int icehSequence = icehWorldCommandBase + 5; // wtVarInt: unsigned varint
        public const int icehSequenceAck = icehWorldCommandBase + 6; // wtVarInt: unsigned varint

        public const int icehMonitoringBase = 1000;         // 1100..1199 monitor

        public const int icehLogLine = 1200;                // <string>
        public const int icehLogLevel = 1201;               // <uint32: varint>
        public const int icehLogSource = 1202;              // <string>

        public const int icehHubManagementBase = 1300;      // ..1399
        public const int icehHubManagementCloseConnection = icehHubManagementBase + 0; // <guid>

        public const int icehModelControlBase = 2000; // ..2099

        // constructor
        public TEventEntry(TConnection aConnection, UInt32 aEventID, string aEventName)
        {
            fConnection = aConnection;
            fEventID = aEventID;
            fEventName = aEventName;
        }

        ~TEventEntry()
        {
            CloseRequests();
        }

        public void CloseRequests()
        {
            // close all existing requests
            lock (fRequests)
            {
                foreach (var r in fRequests)
                    r.Value.Close(this);
            }
        }

        // private methods
        public void HandleEvent(TByteBuffer aBuffer)
        {
            if (OnEvent != null)
            {
                // todo: cannot use simple call because we have to reset the TByteBuffer state in between calls: OnEvent(this, aBuffer);
                var backupOffset = aBuffer.Offset;
                foreach(var handler in OnEvent.GetInvocationList())
                {
                    handler.DynamicInvoke(this, aBuffer);
                    aBuffer.Offset = backupOffset;
                }
            }
            else
            {
                string eventString = "";
                int action = -1;
                string attribute = "";
                Guid streamID = Guid.Empty;
                TStreamCacheEntry sce; // streamCacheEntry
                UInt32 logLevel = 0;
                string logSource = "";
                while (aBuffer.Remaining > 0)
                {
                    UInt32 fieldInfo = aBuffer.BB_read_uint32();
                    switch (fieldInfo)
                    {
                        // int string
                        case (icehIntString << 3) | TByteBuffer.wtVarInt:
                            int eventInt = aBuffer.BB_read_int32();
                            OnIntString?.Invoke(this, eventInt, eventString);
                            break;
                        case (icehIntStringPayload << 3) | TByteBuffer.wtLengthDelimited:
                            eventString = aBuffer.BB_read_string();
                            break;
                        // string
                        case (icehString << 3) | TByteBuffer.wtLengthDelimited:
                            eventString = aBuffer.BB_read_string();
                            OnString?.Invoke(this, eventString);
                            break;
                        // change object
                        case (icehChangeObjectAction << 3) | TByteBuffer.wtVarInt:
                            action = aBuffer.BB_read_int32();
                            break;
                        case (icehChangeObjectAttribute << 3) | TByteBuffer.wtLengthDelimited:
                            attribute = aBuffer.BB_read_string();
                            break;
                        case (icehChangeObject << 3) | TByteBuffer.wtVarInt:
                            int objectID = aBuffer.BB_read_int32();
                            OnChangeObject?.Invoke(this, action, objectID, attribute);
                            break;
                        // streams
                        case (icehStreamHeader << 3) | TByteBuffer.wtLengthDelimited:
                            string streamName = aBuffer.BB_read_string();
                            if (OnStreamCreate != null)
                            {
                                Stream stream = OnStreamCreate(this, streamName);
                                if (stream != null)
                                {
                                    if (fStreamCache.TryGetValue(streamID, out sce))
                                    {
                                        sce.Stream = stream;
                                        sce.StreamName = streamName;
                                    }
                                    else
                                        fStreamCache.Add(streamID, new TStreamCacheEntry(streamName, stream));
                                }
                            }
                            break;
                        case (icehStreamBody << 3) | TByteBuffer.wtLengthDelimited:
                            byte[] block = aBuffer.BB_read_bytes();
                            if (fStreamCache.TryGetValue(streamID, out sce))
                                sce.Stream.Write(block, 0, block.Length);
                            break;
                        case (icehStreamEnd << 3) | TByteBuffer.wtVarInt:
                            bool cancel = aBuffer.BB_read_bool();
                            if (fStreamCache.TryGetValue(streamID, out sce))
                            {
                                OnStreamEnd?.Invoke(this, sce.StreamName, sce.Stream, cancel);
                                sce.Stream.Close();
                                fStreamCache.Remove(streamID);
                            }
                            break;
                        case (icehStreamID << 3) | TByteBuffer.wtLengthDelimited:
                            streamID = aBuffer.BB_read_guid();
                            break;
                        case (icehLogLevel << 3) | TByteBuffer.wtVarInt:
                            logLevel = aBuffer.BB_read_uint32();
                            break;
                        case (icehLogSource << 3) | TByteBuffer.wtLengthDelimited:
                            logSource = aBuffer.BB_read_string();
                            break;
                        case (icehLogLine << 3) | TByteBuffer.wtLengthDelimited:
                            var logLine = aBuffer.BB_read_string();
                            OnLog?.Invoke(this, logLine, logLevel, logSource);
                            break;
                        default:
                            if (OnTag != null)
                                OnTag(this, fieldInfo, aBuffer);
                            else
                                aBuffer.BB_read_skip((int)(fieldInfo & 7));
                            break;
                    }
                }
            }
        }

        public void HandleSubAndPub(UInt32 aCommand)
        {
            switch (aCommand)
            {
                case TConnection.icehSubscribe:
                    fSubscribers = true;
                    break;
                case TConnection.icehPublish:
                    fPublishers = true;
                    break;
                case TConnection.icehUnsubscribe:
                    fSubscribers = false;
                    break;
                case TConnection.icehUnpublish:
                    fPublishers = false;
                    break;
            }
            OnSubAndPub?.Invoke(this, aCommand);
        }

        private void SignalSubscribe()
        {
            fConnection.WriteCommand(
                TByteBuffer.BB_tag_string(TConnection.icehEventName, EventName),
                TByteBuffer.BB_tag_uint32(TConnection.icehSubscribe, EventID));
        }

        private void SignalPublish()
        {
            fConnection.WriteCommand(
                TByteBuffer.BB_tag_string(TConnection.icehEventName, EventName),
                TByteBuffer.BB_tag_uint32(TConnection.icehPublish, EventID));
        }

        private void SignalUnSubscribe()
        {
            fConnection.WriteCommand(
                TByteBuffer.BB_tag_uint32(TConnection.icehUnsubscribe, EventID));
        }

        private void SignalUnPublish()
        {
            fConnection.WriteCommand(
                TByteBuffer.BB_tag_uint32(TConnection.icehUnpublish, EventID));
        }

        private void SignalRespond()
        {
            fConnection.WriteCommand(
                TByteBuffer.BB_tag_string(TConnection.icehEventName, EventName),
                TByteBuffer.BB_tag_uint32(TConnection.icehRespond, EventID));
        }

        private void SignalUnRespond()
        {
            fConnection.WriteCommand(
                TByteBuffer.BB_tag_uint32(TConnection.icehUnrespond, EventID));
        }

        private void SignalRegisterEvent()
        {
            fConnection.WriteCommand(
                TByteBuffer.BB_tag_string(TConnection.icehEventName, EventName),
                TByteBuffer.BB_tag_uint32(TConnection.icehRegisterEvent, EventID));
        }

        private void SignalRequest(Guid aRequestID, TByteBuffer aData, Int64 aRequestTimeout)
        {
            fConnection.WriteCommand(
                TByteBuffer.BB_tag_uint32(TConnection.icehEventID, EventID),
                TByteBuffer.BB_tag_guid(TConnection.icehRequestID, aRequestID),
                aRequestTimeout > 0 ? TByteBuffer.BB_tag_int64(TConnection.icehRequestTimeout, aRequestTimeout) : new byte[0],
                TByteBuffer.BB_tag_bytes(TConnection.icehRequest, aData.Buffer));
        }

        // to be called from onResponse
        private void SignalRequestError(Guid aRequestID, int aRequestError)
        {
            fConnection.WriteCommand(
                TByteBuffer.BB_tag_uint32(TConnection.icehEventID, EventID),
                TByteBuffer.BB_tag_guid(TConnection.icehRequestID, aRequestID),
                TByteBuffer.BB_tag_int32(TConnection.icehRequestError, aRequestError));
        }

        // to be called from onResponse
        private void SignalResponderHeartbeat(Guid aRequestID, int aResponseHeartbeat)
        {
            fConnection.WriteCommand(
                TByteBuffer.BB_tag_uint32(TConnection.icehEventID, EventID),
                TByteBuffer.BB_tag_guid(TConnection.icehRequestID, aRequestID),
                TByteBuffer.BB_tag_int32(TConnection.icehResponderHeartbeat, aResponseHeartbeat));
        }

        //private void SetOnReuest(TOnRequest aValue) {}

        // fields
        private TConnection fConnection;
        private UInt32 fEventID;
        private string fEventName;

        private bool fIsSubscribed = false;
        private bool fIsPublished = false;
        private bool fIsResponder = false;
        private bool fIsRegistered = false;

        private bool fSubscribers = false;
        private bool fPublishers = false;
        private bool fResponders = false;
        private Dictionary<Guid, TRequest> fRequests = new Dictionary<Guid, TRequest>();

        private Dictionary<Guid, TStreamCacheEntry> fStreamCache = new Dictionary<Guid, TStreamCacheEntry>();

        // properties
        public TConnection Connection { get { return fConnection; } }
        public string EventName { get { return fEventName; } }
        public UInt32 EventID { get { return fEventID; } }
        public bool IsSubscribed { get { return fIsSubscribed; } }
        public bool IsPublished { get { return fIsPublished; } }
        public bool IsResponder { get { return fIsResponder; } }
        public bool Subscribers { get { return fSubscribers; } }
        public bool Publishers { get { return fPublishers; } }
        public bool Responders { get { return fResponders; } set { fResponders = value; } }

        public TEventEntry Reconnect()
        {
            if (fIsPublished)
                SignalPublish();
            if (fIsSubscribed)
                SignalSubscribe();
            if (fIsResponder)
                SignalRespond();
            // discard running requests: they are lost!
            CloseRequests();
            return this;
        }

        public TEventEntry Publish()
        {
            if (!fIsPublished)
            {
                SignalPublish();
                fIsPublished = true;
            }
            return this;
        }

        public TEventEntry Subscribe()
        {
            if (!fIsSubscribed)
            {
                SignalSubscribe();
                fIsSubscribed = true;
            }
            return this;
        }

        public TEventEntry UnPublish()
        {
            if (fIsPublished)
            {
                SignalUnPublish();
                fIsPublished = false;
            }
            return this;
        }

        public TEventEntry UnSubscribe()
        {
            if (fIsSubscribed)
            {
                SignalUnSubscribe();
                fIsSubscribed = false;
            }
            return this;
        }

        public TEventEntry Respond()
        {
            if (!fIsResponder)
            {
                SignalRespond();
                fIsResponder = true;
            }
            return this;
        }

        public TEventEntry UnRespond()
        {
            if (fIsResponder)
            {
                SignalUnRespond();
                fIsResponder = false;
            }
            return this;
        }

        public TEventEntry RegisterEvent()
        {
            if (!fIsRegistered)
            {
                SignalRegisterEvent();
                fIsRegistered = true;
            }
            return this;
        }

        public void SignalEvent(params byte[][] aPayloads)
        {
            Publish();
            byte[] bufferEventID = TByteBuffer.BB_uint16((UInt16)fEventID);
            byte[] payload = TByteBuffer.BB_join(aPayloads);
            Int64 size = bufferEventID.Length + payload.Length;
            fConnection.WritePacket(TByteBuffer.BB_join(
                new byte[1] { TConnection.imbMagic },
                TByteBuffer.BB_int64(size),
                bufferEventID,
                payload
                ));
        }

        // signal event
        public void SignalChangeObject(int aAction, int aObjectID, string aAttribute = "")
        {
            SignalEvent(
                TByteBuffer.BB_tag_int32(icehChangeObjectAction, aAction),
                TByteBuffer.BB_tag_string(icehChangeObjectAttribute, aAttribute),
                TByteBuffer.BB_tag_int32(icehChangeObject, aObjectID)
                );
        }

        public void SignalString(string aString)
        {
            SignalEvent(
                TByteBuffer.BB_tag_string(icehString, aString)
                );
        }

        public void SignalString(byte[] aStringInUTF8Bytes) // string in UTF8 bytes
        {
            SignalEvent(
                TByteBuffer.BB_tag_bytes(icehString, aStringInUTF8Bytes)
                );
        }

        public void SignalIntString(int aInt, string aString)
        {
            SignalEvent(
                TByteBuffer.BB_tag_string(icehIntStringPayload, aString),
                TByteBuffer.BB_tag_int32(icehIntString, aInt)
                );
        }

        public void SignalStream(string aName, Stream aStream)
        {
            byte[] bufferStreamID = TByteBuffer.BB_tag_guid(icehStreamID, Guid.NewGuid());
            // header
            SignalEvent(bufferStreamID, TByteBuffer.BB_tag_string(icehStreamHeader, aName));
            // body
            byte[] buffer = new byte[imbMaxStreamBodyBuffer];
            int readSize = aStream.Read(buffer, 0, imbMaxStreamBodyBuffer);
            while (readSize > 0)
            {
                if (readSize < buffer.Length)
                    Array.Resize<byte>(ref buffer, readSize);
                SignalEvent(bufferStreamID, TByteBuffer.BB_tag_bytes(icehStreamBody, buffer));
                readSize = aStream.Read(buffer, 0, readSize);
            }
            // end
            SignalEvent(bufferStreamID, TByteBuffer.BB_tag_bool(icehStreamEnd, readSize != 0));
        }

        public void SignalLogEntry(string aLine, UInt32 aLevel, string aSource = "")
        {
            if (aSource != "")
                SignalEvent(
                    TByteBuffer.BB_tag_uint32(icehLogLevel, aLevel),
                    TByteBuffer.BB_tag_string(icehLogSource, aSource),
                    TByteBuffer.BB_tag_string(icehLogLine, aLine)
                    );
            else
                SignalEvent(
                    TByteBuffer.BB_tag_uint32(icehLogLevel, aLevel),
                    TByteBuffer.BB_tag_string(icehLogLine, aLine)
                    );
        }


        // handlers
        public delegate void TOnChangeObject(TEventEntry aEventEntry, Int32 aAction, Int32 aObjectID, string aAttribute);
        public event TOnChangeObject OnChangeObject = null;

        public delegate void TOnString(TEventEntry aEventEntry, string aString);
        public event TOnString OnString = null;

        public delegate void TOnIntString(TEventEntry aEventEntry, int aInt, string aString);
        public event TOnIntString OnIntString = null;

        public delegate void TOnTag(TEventEntry aEventEntry, UInt32 aFieldInfo, TByteBuffer aPayload);
        public event TOnTag OnTag = null;

        public delegate Stream TOnStreamCreate(TEventEntry aEventEntry, string aName);
        public event TOnStreamCreate OnStreamCreate = null;

        public delegate void TOnStreamEnd(TEventEntry aEventEntry, string aName, Stream stream, bool aCancel);
        public event TOnStreamEnd OnStreamEnd = null;

        public delegate void TOnSubAndPub(TEventEntry aEventEntry, UInt32 aCommand);
        public event TOnSubAndPub OnSubAndPub = null;

        public Func<TEventEntry, Guid, TByteBuffer, byte[]> OnRequest { get; set; } = null;

        public delegate void TOnLog(TEventEntry aEventEntry, string aLine, UInt32 aLevel, string aSource);
        public event TOnLog OnLog = null;

        //public Action<TEventEntry, TByteBuffer> OnEvent = null; // overrules all other tag events, can only call single entry 
        public delegate void TOnEvent(TEventEntry aEventEntry, TByteBuffer aBuffer);
        public event TOnEvent OnEvent = null;

        public object Tag { get; set; }

        public TByteBuffer Request(TByteBuffer aRequestData, out int aRequestError, Int64 aRequestTimeout = 0)
        {
            var request = new TRequest(Guid.NewGuid(), null);
            try
            {
                RegisterEvent();
                lock (fRequests)
                {
                    fRequests.Add(request.RequestID, request);
                    SignalRequest(request.RequestID, aRequestData, aRequestTimeout);
                }
                // block
                if (request.RequestComplete.WaitOne(-1)) // infinite
                {
                    if (request.RequestError == TRequest.reBusy)
                        request.RequestError = TRequest.reOK;
                }
                else
                {
                    if (request.RequestError == TRequest.reBusy)
                        request.RequestError = TRequest.reTimedOut;
                }
                aRequestError = request.RequestError;
                return request.Responses;
            }
            finally
            {
                lock (fRequests)
                {
                    fRequests.Remove(request.RequestID);
                }
            }
        }


        public void Request(TByteBuffer aRequestData, Action<TEventEntry, TRequest> aResponderHandler, Int64 aRequestTimeout)
        {
            RegisterEvent();
            var request = new TRequest(Guid.NewGuid(), aResponderHandler);
            lock(fRequests)
            {
                fRequests.Add(request.RequestID, request);
                SignalRequest(request.RequestID, aRequestData, aRequestTimeout);
            }
        }

        public void HandleRequestError(Guid aRequestID, int aRequestError)
        {
            TRequest doRequest = null;
            lock (fRequests)
            {
                if (fRequests.TryGetValue(aRequestID, out TRequest request))
                {
                    request.RequestError = aRequestError;
                    if (aRequestError == TRequest.reTimedOut)
                    {
                        if (request.OnResponse != null)
                        {
                            doRequest = request;
                            fRequests.Remove(aRequestID);
                        }
                        else
                            request.RequestComplete.Set();
                    }
                }
            }
            // outside lock
            doRequest?.OnResponse(this, doRequest);
        }

        public void HandleResponse(Guid aRequestID, byte[] aRequestData, bool aEndOfResponse)
        {
            TRequest doRequest = null;
            lock (fRequests)
            {
                if (fRequests.TryGetValue(aRequestID, out TRequest request))
                {
                    request.AddResponse(aRequestData);
                    if (aEndOfResponse)
                    {
                        // check and change status
                        if (request.RequestError == TRequest.reBusy)
                            request.RequestError = TRequest.reOK;
                        if (request.OnResponse != null)
                        {
                            doRequest = request;
                            fRequests.Remove(aRequestID);
                        }
                        else
                        {
                            request.RequestComplete.Set();
                        }
                    }
                }
            }
            // outside lock
            doRequest?.OnResponse(this, doRequest);
        }
    }

    public abstract class TConnection
    {
        public const string imbDefaultRemoteHost = "vps54128.public.cloudvps.com"; // "localhost"; // 

        public const string imbDefaultPrefix = "nl.imb";

        public const byte imbMagic = 0xFE;

        public const int imbMinimumPacketSize = 16;
        public const int imbMaximumPayloadSize = 10 * 1024 * 1024;
        public const int imbSocketDefaultLingerTimeInSec = 2; // in sec

        public const int imbReconnectSleepTime = 1000; // msec
        public const int imbMaxReconnectSleepTime = 10*imbReconnectSleepTime; // msec

        public const int imbHeartbeatIntervalMS = 60000; // msec

        // client state
        public const int icsUninitialized = 0;
        public const int icsInitialized = 1;
        public const int icsClient = 2;
        public const int icsHub = 3;
        public const int icsEnded = 4;
        public const int icsTimer = 10;
        public const int icsGateway = 100;

        // command tags
        public const int icehRemark = 1;                      // <string>
        public const int icehSubscribe = 2;                   // <uint32: varint>
        public const int icehPublish = 3;                     // <uint32: varint>
        public const int icehUnsubscribe = 4;                 // <uint32: varint>
        public const int icehUnpublish = 5;                   // <uint32: varint>
        public const int icehSetEventIDTranslation = 6;       // <uint32: varint>
        public const int icehEventName = 7;                   // <string>
        public const int icehEventID = 8;                     // <uint32: varint>
        public const int icehAutoSubscribe = 9;               // <uint32: varint>
        public const int icehRegisterEvent = 10;              // <uint32: varint>

        public const int icehUniqueClientID = 11;             // <guid>
        public const int icehHubID = 12;                      // <guid>
        public const int icehModelName = 13;                  // <string>
        public const int icehModelID = 14;                    // <int32: varint> ?
        public const int icehReconnectable = 15;              // <bool: varint> ?
        public const int icehState = 16;                      // <uint32: varint>
        public const int icehEventNameFilter = 17;            // <string>
        public const int icehClose = 21;                      // <bool: varint>
        public const int icehReconnect = 22;                  // <guid>
        public const int icehReconnected = 23;                // <bool: varint>

        // request/response
        public const int icehRespond = 31;                    // <uint32: varint>    like icehSubscribe but register as responder
        public const int icehUnrespond = 32;                  // <uint32: varint>    like icehUnsubscribe but un-register as responder
        public const int icehRequestID = 33;                  // <guid>
        public const int icehRequest = 34;                    // <bytes>
        public const int icehRequestError = 35;               // <int32: varint>
        public const int icehResponse = 36;                   // <bytes>
        public const int icehResponseEnd = 37;                // <bytes>
        public const int icehResponderHeartbeat = 38;         // <int32: varint>
        public const int icehRequestTimeout = 39;             // <uint32: varint>

        public const UInt32 imbInvalidEventID = 0xFFFFFFFF;

        // connection errors
        const int ceInvalidPacketSize = -2;
        const int ceInvalidMagic = -1;
        const int ceOK = 0;
        const int ceQuickReconnect = 1;
        const int ceFullReconnect = 2;
        
        // abstract methods to override
        protected abstract bool GetConnected();
        protected abstract bool SetupConnection();
        protected abstract void FinishConnection();

        protected Thread fReaderThread = null;

        protected virtual void SetConnected(bool aValue)
        {
            if (aValue)
            {
                if (!Connected)
                {
                    if (SetupConnection())
                    {
                        fReaderThread = new Thread(ReadPackets) { Name = "IMB reader "+ this.GetType().Name };
                        fReaderThread.Start();
                        // send connect info
                        SignalConnectInfo(fModelName, fModelID, fEventNameFilter);
                        // wait for unique client id as a signal that we are connected
                        WaitForConnected();
                    }
                }
            }
            else
            {
                if (Connected)
                {
                    WriteCommand(TByteBuffer.BB_tag_bool(icehClose, false));
                    fReaderThread.Abort();
                    fReaderThread = null;
                    FinishConnection();
                }
                else
                {
                    // force reader thread to end to disable possible reconnect in progress
                    fReaderThread?.Abort();
                    fReaderThread = null;
                }
            }
        }


        protected abstract int ReadBytes(byte[] aBuffer, int aOffset, int aLimit);
        public abstract void WritePacket(byte[] aPacket);

        public TConnection(string aModelname, int aModelID, string aPrefix, string aEventNameFilter)
        {
            fModelName = aModelname;
            fModelID = aModelID;
            fPrefix = aPrefix;
            fEventNameFilter = aEventNameFilter;
        }

        ~TConnection()
        {
            // disconnect
            Connected = false;
            // cleanup
            Reset();
        }

        protected virtual bool HandleReconnect()
        {
            // we are called from the reader thread so we can and must do specific reading here
            // try to re-establish socket
            // signal we want to reconnect to our old connection
            // if not successfull exit because otherwise we will have to use a new private connection, re-subscribe/publish/respond etc..
            Debug.WriteLine(">> Starting reconnect");
            var reconnect = OnDisconnect?.Invoke(this) ?? true;
            if (reconnect)
            {
                var sleepTime = imbReconnectSleepTime;
                do
                {
                    try { SetupConnection(); } catch (Exception) { } // for now: do nothing just try to re-connect
                    if (!Connected && (fReaderThread != null))
                    {
                        Thread.Sleep(sleepTime);
                        // adjust sleep time
                        sleepTime = sleepTime * 2;
                        if (sleepTime > imbMaxReconnectSleepTime)
                            sleepTime = imbMaxReconnectSleepTime;
                    }
                }
                while (!Connected && fReaderThread != null);
                if (fReaderThread != null)
                {
                    // signal we want to reconnect to our old connection
                    WriteCommand(
                        TByteBuffer.BB_tag_string(icehModelName, fModelName),
                        TByteBuffer.BB_tag_int32(icehModelID, fModelID),
                        TByteBuffer.BB_tag_uint32(icehState, icsClient),
                        TByteBuffer.BB_tag_bool(icehReconnectable, true),
                        TByteBuffer.BB_tag_string(icehEventNameFilter, fEventNameFilter),
                        TByteBuffer.BB_tag_guid(icehReconnect, fUniqueClientID)); // trigger
                    // wait for reconnect status or disconnect
                    var res = WaitForReconnected();
                    if (Connected)
                    {
                        if (res == ceQuickReconnect)
                        {
                            Debug.WriteLine("Reconnected");
                            return OnRecover?.Invoke(this, true) ?? true; // quick reconnect: we are using the same connection hub side so no data shpuld be lost
                        }
                        else if (res == ceFullReconnect)
                        {
                            Debug.WriteLine("> Hub does not allow simple reconnect => rebuild connection");
                            // cleanup remote event id because they are all be invalid now: we have to rebuild all event info
                            fRemoteEventEntries.Clear();
                            // reconnect all events
                            foreach (var ee in fLocalEventEntries)
                                ee.Reconnect();
                            return OnRecover?.Invoke(this, false) ?? true; // slow reconnect: essentially we have a completely new connection with only the same unique client id
                        }
                    }
                }
                else
                {
                    Debug.WriteLine("// Terminated, aborted reconnect");
                }
            }
            // if we come here reconnect failed
            return false;
        }

        public void Reset()
        {
            fLocalEventEntries.Clear();
            fRemoteEventEntries.Clear();
            fUniqueClientID = Guid.Empty;
        }

        // fields
        protected string fModelName;
        protected int fModelID;
        protected string fPrefix;
        protected List<TEventEntry> fLocalEventEntries = new List<TEventEntry>();
        protected Dictionary<UInt32, TEventEntry> fRemoteEventEntries = new Dictionary<UInt32, TEventEntry>();
        protected Guid fUniqueClientID;
        protected Guid fHubID;
        protected string fEventNameFilter;

        public string ModelName { get { return fModelName; } }
        public int ModelID { get { return fModelID; } }
        public string Prefix { get { return fPrefix; } }
        public Guid UniqueClientID { get { return fUniqueClientID; } }
        public Guid HubID { get { return fHubID; } }

        public delegate bool TOnDisconnect(TConnection aConnection);
        public event TOnDisconnect OnDisconnect = null;
        public delegate bool TOnRecover(TConnection aConnection, bool aQuick);
        public event TOnRecover OnRecover = null;
        public delegate void TOnException(TConnection aConnection, Exception aException);
        public event TOnException OnException = null;
        public delegate void TOnSubAndPub(TConnection aConnection, string aEventName, UInt32 aCommand);
        public event TOnSubAndPub OnSubAndPub = null;

        public string MonitorEventName
        {
            get
            {
                if (fHubID.CompareTo(Guid.Empty) == 0)
                    return "";
                else
                    return "Hubs." + fHubID.ToString("N").ToUpper() + ".Monitor";
            }
        }

        public string PrivateEventName
        {
            get
            {
                if (fUniqueClientID.CompareTo(Guid.Empty) == 0)
                    return "";
                else
                    return "Clients." + fUniqueClientID.ToString("N").ToUpper() + ".Private";
            }
        }

        protected void WaitForConnected(int aSpinCount = 20)
        {
            while (fUniqueClientID.CompareTo(Guid.Empty) == 0 && aSpinCount != 0)
            {
                Thread.Sleep(500);
                aSpinCount--;
            }
        }

        protected int HandleCommand(TByteBuffer aBuffer)
        {
            int res = ceOK; // sentinel -> handled ok
            TEventEntry eventEntry = null;
            string eventName = "";
            UInt32 localEventID = imbInvalidEventID;
            UInt32 eventID = imbInvalidEventID;
            Guid requestID = Guid.Empty;
            Int32 requestError;
            byte[] requestBytes;
            // process tags
            while (aBuffer.Remaining > 0)
            {
                UInt32 fieldInfo = aBuffer.BB_read_uint32();
                switch (fieldInfo)
                {
                    case (icehSubscribe << 3) | TByteBuffer.wtVarInt:
                        UInt32 remoteEventID = aBuffer.BB_read_uint32();
                        if (fRemoteEventEntries.TryGetValue(remoteEventID, out eventEntry))
                            eventEntry.HandleSubAndPub(icehSubscribe);
                        else
                            OnSubAndPub?.Invoke(this, eventName, icehSubscribe);
                        break;
                    case (icehPublish << 3) | TByteBuffer.wtVarInt:
                        remoteEventID = aBuffer.BB_read_uint32();
                        if (fRemoteEventEntries.TryGetValue(remoteEventID, out eventEntry))
                            eventEntry.HandleSubAndPub(icehPublish);
                        else
                            OnSubAndPub?.Invoke(this, eventName, icehPublish);
                        break;
                    case (icehUnsubscribe << 3) | TByteBuffer.wtVarInt:
                        eventName = "";
                        remoteEventID = aBuffer.BB_read_uint32();
                        if (fRemoteEventEntries.TryGetValue(remoteEventID, out eventEntry))
                            eventEntry.HandleSubAndPub(icehUnsubscribe);
                        else
                            OnSubAndPub?.Invoke(this, eventName, icehUnsubscribe);
                        break;
                    case (icehUnpublish << 3) | TByteBuffer.wtVarInt:
                        eventName = "";
                        remoteEventID = aBuffer.BB_read_uint32();
                        if (fRemoteEventEntries.TryGetValue(remoteEventID, out eventEntry))
                            eventEntry.HandleSubAndPub(icehUnpublish);
                        else
                            OnSubAndPub?.Invoke(this, eventName, icehUnpublish);
                        break;
                    case (icehAutoSubscribe << 3) | TByteBuffer.wtVarInt:
                        remoteEventID = aBuffer.BB_read_uint32();
                        if (fRemoteEventEntries.TryGetValue(remoteEventID, out eventEntry))
                            eventEntry.HandleSubAndPub(icehAutoSubscribe);
                        else
                            OnSubAndPub?.Invoke(this, eventName, icehAutoSubscribe);
                        break;
                    case (icehEventName << 3) | TByteBuffer.wtLengthDelimited:
                        eventName = aBuffer.BB_read_string();
                        break;
                    case (icehEventID << 3) | TByteBuffer.wtVarInt:
                        eventID = aBuffer.BB_read_uint32();
                        break;
                    case (icehSetEventIDTranslation << 3) | TByteBuffer.wtVarInt:
                        remoteEventID = aBuffer.BB_read_uint32();
                        localEventID = eventID;
                        lock (fLocalEventEntries)
                        {
                            if (fRemoteEventEntries.ContainsKey(remoteEventID))
                                fRemoteEventEntries.Remove(remoteEventID);
                            if (localEventID < fLocalEventEntries.Count)
                            {
                                eventEntry = fLocalEventEntries[(int)localEventID];
                                fRemoteEventEntries.Add(remoteEventID, eventEntry);
                            }
                            // else local event id is invalid so invalidate remote event id -> keep removed
                        }
                        break;
                    case (icehClose << 3) | TByteBuffer.wtVarInt:
                        aBuffer.BB_read_bool();
                        FinishConnection(); 
                        break;
                    case (icehReconnected << 3) | TByteBuffer.wtVarInt:
                        res = aBuffer.BB_read_bool() ? ceQuickReconnect : ceFullReconnect;
                        break;
                    case (icehHubID << 3) | TByteBuffer.wtLengthDelimited:
                        fHubID = aBuffer.BB_read_guid();
                        break;
                    case (icehUniqueClientID << 3) | TByteBuffer.wtLengthDelimited:
                        fUniqueClientID = aBuffer.BB_read_guid();
                        break;

                    case (icehRespond << 3) | TByteBuffer.wtVarInt: // <uint32: varint>    like icehSubscribe but register as responder
                        remoteEventID = aBuffer.BB_read_uint32();
                        if (fRemoteEventEntries.TryGetValue(remoteEventID, out eventEntry))
                            eventEntry.Responders = true;
                        break;
                    case (icehUnrespond << 3) | TByteBuffer.wtVarInt: // <uint32: varint>    like icehUnsubscribe but un-register as responder
                        remoteEventID = aBuffer.BB_read_uint32();
                        if (fRemoteEventEntries.TryGetValue(remoteEventID, out eventEntry))
                            eventEntry.Responders = false;
                        break;
                    case (icehRequestID << 3) | TByteBuffer.wtLengthDelimited: // <guid>
                        requestID = aBuffer.BB_read_guid();
                        break;
                    case (icehRequest << 3) | TByteBuffer.wtLengthDelimited: // <bytes>
                        var requestBuffer = new TByteBuffer(aBuffer.BB_read_bytes());
                        remoteEventID = eventID;
                        if (fRemoteEventEntries.TryGetValue(remoteEventID, out eventEntry))
                        {
                            // start thread to handle request
                            new Thread(() =>
                                {
                                    byte[] responseData;
                                    // always answer request
                                    responseData = eventEntry.OnRequest != null ? eventEntry.OnRequest(eventEntry, requestID, requestBuffer) : new byte[0];
                                    SignalResponse(eventEntry.EventID, requestID, responseData, true);
                                }) { Name = "Handle request " + requestID.ToString() }.Start();
                        }
                        // else: we cannot answer because we do not known the event id to answer with.. will become a time out; but should not happen anyway!
                        break;
                    case (icehRequestError << 3) | TByteBuffer.wtVarInt: // <int32: varint>
                        requestError = aBuffer.BB_read_int32();
                        remoteEventID = eventID;
                        if (fRemoteEventEntries.TryGetValue(remoteEventID, out eventEntry))
                            eventEntry.HandleRequestError(requestID, requestError);
                        break;
                    case (icehResponse << 3) | TByteBuffer.wtLengthDelimited: // <bytes>
                        requestBytes = aBuffer.BB_read_bytes();
                        remoteEventID = eventID;
                        if (fRemoteEventEntries.TryGetValue(remoteEventID, out eventEntry))
                            eventEntry.HandleResponse(requestID, requestBytes, false);
                        break;
                    case (icehResponseEnd << 3) | TByteBuffer.wtLengthDelimited: // <bytes>
                        requestBytes = aBuffer.BB_read_bytes();
                        remoteEventID = eventID;
                        if (fRemoteEventEntries.TryGetValue(remoteEventID, out eventEntry))
                            eventEntry.HandleResponse(requestID, requestBytes, true);
                        break;
                    default:
                        aBuffer.BB_read_skip((int)fieldInfo & 7);
                        break;
                }
            }
            return res;
        }

        protected void SignalConnectInfo(string aModelName, int aModelID, string aEventNameFilter)
        {
            WriteCommand(
                TByteBuffer.BB_tag_string(icehModelName, aModelName),
                TByteBuffer.BB_tag_int32(icehModelID, aModelID),
                TByteBuffer.BB_tag_uint32(icehState, icsClient),
                TByteBuffer.BB_tag_bool(icehReconnectable, true),
                TByteBuffer.BB_tag_string(icehEventNameFilter, aEventNameFilter),
                TByteBuffer.BB_tag_guid(icehUniqueClientID, fUniqueClientID)); // trigger
        }

        private void SignalResponse(UInt32 aEventID, Guid aRequestID, byte[] aData, bool aEndOfResponse)
        {
            WriteCommand(
                TByteBuffer.BB_tag_uint32(icehEventID, aEventID),
                TByteBuffer.BB_tag_guid(icehRequestID, aRequestID),
                TByteBuffer.BB_tag_bytes((UInt32)(aEndOfResponse ? icehResponseEnd : icehResponse), aData));
        }

        public void SignalHeartbeat(string aRemark = "")
        {
            if (aRemark == "")
                WritePacket(new byte[2] { TConnection.imbMagic, 0 });
            else
                WriteCommand(TByteBuffer.BB_tag_string(icehRemark, aRemark));
        }

        protected Thread fHeartbeatThread = null;

        protected void StartHeartbeat()
        {
            if (fHeartbeatThread != null)
                fHeartbeatThread.Abort();
            fHeartbeatThread = new Thread(() =>
            {
                while (Connected)
                {
                    SignalHeartbeat();
                    if (Connected)
                        Thread.Sleep(imbHeartbeatIntervalMS);
                }
            })
            { Name = "IMB heartbeat" };
            fHeartbeatThread.Start();
        }

        protected void StopHeartbeat()
        {
            if (fHeartbeatThread != null)
            {
                fHeartbeatThread.Abort();
                fHeartbeatThread = null;
            }
        }

        protected void ReadPackets() // event reader thread loop
        {
            // process IMB packets
            TByteBuffer packet = new TByteBuffer(imbMinimumPacketSize);
            do
            {
                try
                {
                    if (Connected)
                    {
                        if (ReadSinglePacket(packet) < ceOK)
                            FinishConnection(); // read error, force reconnect by closing the connection
                    }
                    else
                    {
                        if (!HandleReconnect())
                            return; // if we cannot reconnect: stop
                    }
                }
                catch (IOException)
                {
                    FinishConnection(); // IO error, force reconnect by closing the connection
                }
                catch (ThreadAbortException)
                {
                    Thread.ResetAbort();
                    return; // we are given a signal to stop
                }
                catch (Exception e)
                {
                    OnException?.Invoke(this, e);
                }
            } while (fReaderThread != null); // endless loop till no reconnect is possible or explicitly told to exit (by thread abort exception) or setting fReaderThread to null
        }

        protected int ReadSinglePacket(TByteBuffer packet)
        {
            packet.Offset = 0;
            packet.Limit = imbMinimumPacketSize;
            int receivedBytes = ReadBytes(packet.Buffer, packet.Offset, packet.Limit);
            if (receivedBytes == imbMinimumPacketSize)
            {
                while (packet.FirstByte != imbMagic)
                {
                    TByteBuffer oneByte = new TByteBuffer(1);
                    if (ReadBytes(oneByte.Buffer, oneByte.Offset, oneByte.Limit) == 1)
                        packet.ShiftLeft(oneByte.FirstByte);
                    else
                        return ceInvalidMagic;
                }
                packet.BB_read_skip_bytes(1);
                // we have magic at the first byte
                int size = (int)packet.BB_read_int64();
                int extraBytesOffset = packet.Limit;
                packet.Limit = packet.Offset + Math.Abs(size);
                if (packet.Limit - extraBytesOffset > 0)
                {
                    receivedBytes = ReadBytes(packet.Buffer, extraBytesOffset, packet.Limit);
                    // todo: check if received number bytes is ok
                }
                if (size > 0)
                {
                    // event
                    UInt16 eventID = packet.BB_read_uint16();
                    if (fRemoteEventEntries.TryGetValue(eventID, out TEventEntry eventEntry))
                        eventEntry.HandleEvent(packet);
                    return ceOK;
                }
                else
                    // command
                    return HandleCommand(packet);
            }
            else
                return ceInvalidPacketSize;
        }

        protected int WaitForReconnected()
        {
            // we have to receive at least 1 command with icehReconnected else we cannot reconnect and terminate
            // set environment for receiving packet
            var packet = new TByteBuffer(imbMinimumPacketSize);
            // read single commands, wait for status != ceOK: error or reconnected (quick/full)
            var res = ReadSinglePacket(packet);
            while (Connected && res==ceOK)
                res = ReadSinglePacket(packet);
            if (res<ceOK)
                FinishConnection(); // no valid reconnect
            return res;
        }

        public List<TEventEntry> EventEntries { get { return fLocalEventEntries; } }
        public bool Connected { get { return GetConnected(); } set { SetConnected(value); } }

        /*
        [Obsolete("please use Connection.EventEntry(aEventName[, aUsePrefix]).Subscribe() instead.")]
        public TEventEntry Subscribe(string aEventName, bool aUsePrefix = true)
        {
            return EventEntry(aEventName, aUsePrefix).Subscribe();
        }

        [Obsolete("please use Connection.EventEntry(aEventName[, aUsePrefix]).Publish() instead.")]
        public TEventEntry Publish(string aEventName, bool aUsePrefix = true)
        {
            return EventEntry(aEventName, aUsePrefix).Publish();
        }
        */
        
        public TEventEntry EventEntry(string aEventName, bool aUsePrefix = true, bool aAutoCreate=true)
        {
            string longEventName = aEventName;
            if (aUsePrefix)
                longEventName = fPrefix + "." + longEventName;
            string upperLongEventName = longEventName.ToUpper();
            lock (fLocalEventEntries)
            {
                foreach (TEventEntry eventEntry in fLocalEventEntries)
                {
                    if (eventEntry.EventName.ToUpper().CompareTo(upperLongEventName) == 0)
                        return eventEntry;
                }
                if (aAutoCreate)
                {
                    // if we come here we have not found an existing event entry
                    TEventEntry newEventEntry = new TEventEntry(this, (UInt32)fLocalEventEntries.Count, longEventName);
                    fLocalEventEntries.Add(newEventEntry);
                    return newEventEntry;
                }
                else
                    return null;
            }
        }

        public void WriteCommand(params byte[][] aPayloads)
        {
            byte[] payload = TByteBuffer.BB_join(aPayloads);
            Int64 size = payload.Length;
            WritePacket(TByteBuffer.BB_join(
                new byte[1] { TConnection.imbMagic },
                TByteBuffer.BB_int64(-size),
                payload
                ));
        }

    }

    public class TSocketConnection : TConnection
    {
        public const int imbDefaultSocketRemotePort = 4005;

        public TSocketConnection(
            string aModelName, int aModelID = 0,
            string aPrefix = imbDefaultPrefix,
            string aRemoteHost = imbDefaultRemoteHost, int aRemotePort = imbDefaultSocketRemotePort, string aEventNameFilter = "")
            : base(aModelName, aModelID, aPrefix, aEventNameFilter)
        {
            fRemoteHost = aRemoteHost;
            fRemotePort = aRemotePort;
            Connected = true;
        }

        protected string fRemoteHost;
        protected int fRemotePort;
        protected TcpClient fClient = null;
        protected NetworkStream fDataStream = null;

        protected override bool GetConnected()
        {
            return fClient != null;
        }

        protected override bool SetupConnection()
        {
            fClient = new TcpClient(fRemoteHost, fRemotePort);
            if (Connected)
                fDataStream = fClient.GetStream();
            return Connected;
        }

        protected override void FinishConnection()
        {
            if (fClient != null)
            {
                fClient.Close();
                fClient = null;
            }
            if (fDataStream != null)
            {
                fDataStream.Close();
                fDataStream = null;
            }
        }

        protected override int ReadBytes(byte[] aBuffer, int aOffset, int aLimit)
        {
            if (Connected)
            {
                int totBytesReceived = 0;
                int bytesReceived;
                do
                {
                    bytesReceived = fDataStream.Read(aBuffer, aOffset, aLimit - aOffset);
                    aOffset += bytesReceived;
                    totBytesReceived += bytesReceived;
                }
                while (aLimit - aOffset > 0 && bytesReceived > 0);
                return totBytesReceived;
            }
            else
                return -1;
        }

        public override void WritePacket(byte[] aPacket)
        {
            try
            {
                lock (fDataStream)
                {
                    fDataStream.Write(aPacket, 0, aPacket.Length);
                    if (imbMinimumPacketSize > aPacket.Length)
                    {
                        // send filler bytes
                        var fillerBytes = new byte[imbMinimumPacketSize - aPacket.Length];
                        fDataStream.Write(fillerBytes, 0, fillerBytes.Length);
                    }
                }
            }
            catch { } // todo; ignore all exceptions?
        }

        public bool SetSocketNoDelay(bool aValue)
        {
            if (fClient != null)
            {
                fClient.NoDelay = aValue;
                return true;
            }
            else
                return false;
        }
        public bool GetSocketNoDelay(out bool aValue)
        {
            if (fClient != null)
            {
                aValue = fClient.NoDelay;
                return true;
            }
            else
            {
                aValue = false;
                return false;
            }
        }
        public bool SetSocketKeepAlive(bool aValue)
        {
            if (fClient != null)
            {
                fClient.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, aValue);
                return true;
            }
            else
                return false;
        }
        public bool GetSocketKeepAlive(out bool aValue)
        {
            if (fClient != null)
            {
                aValue = (int)fClient.Client.GetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive) != 0;
                return true;
            }
            else
            {
                aValue = false;
                return false;
            }

        }
        public bool SetSocketTimeOuts(int aRCVTimeOut, int aSNDTimeOut)
        {
            if (fClient != null)
            {
                fClient.ReceiveTimeout = aRCVTimeOut;
                fClient.SendTimeout = aSNDTimeOut;
                return true;
            }
            else
                return false;
        }
        public bool GetSocketTimeOuts(out int aRCVTimeOut, out int aSNDTimeOut)
        {
            if (fClient != null)
            {
                aRCVTimeOut = fClient.ReceiveTimeout;
                aSNDTimeOut = fClient.SendTimeout;
                return true;
            }
            else
            {
                aRCVTimeOut = -1;
                aSNDTimeOut = -1;
                return false;
            }
        }
    }

    public class TTLSConnection : TConnection
    {
        public const int imbDefaultTLSRemotePort = 4443; // todo: change to 4005 when auto detect is build-in to hub

        public TTLSConnection(
            string aCertFile, string aCertFilePassword, string aRootCertFile,
            bool aStrictCertificateCheck,
            string aModelName, int aModelID = 0,
            string aPrefix = imbDefaultPrefix,
            string aRemoteHost = imbDefaultRemoteHost, int aRemotePort = imbDefaultTLSRemotePort, string aEventNameFilter = "")
            : base(aModelName, aModelID, aPrefix, aEventNameFilter)
        {
            fRemoteHost = aRemoteHost;
            fRemotePort = aRemotePort;

            fStrictCertificateCheck = aStrictCertificateCheck;

            X509Certificate clientCertificate = new X509Certificate2(aCertFile, aCertFilePassword);
            fCertificates.Add(clientCertificate);
            if (!aStrictCertificateCheck)
            {
                X509Certificate rootCertificate = X509Certificate.CreateFromCertFile(aRootCertFile);
                fCertificates.Add(rootCertificate);
            }

            Connected = true;
        }

        protected string fRemoteHost;
        protected int fRemotePort;
        protected TcpClient fClient = null;
        protected SslStream fDataStream = null;
        bool fStrictCertificateCheck;
        private X509CertificateCollection fCertificates = new X509CertificateCollection();

        protected override bool GetConnected()
        {
            return fClient != null;
        }

        protected bool CheckRemoteCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            switch (sslPolicyErrors)
            {
                case SslPolicyErrors.RemoteCertificateNameMismatch:
                    Debug.WriteLine("## Server name mismatch");
                    return false;
                case SslPolicyErrors.RemoteCertificateNotAvailable:
                    Debug.WriteLine("## Server's certificate not available");
                    return false;
                case SslPolicyErrors.RemoteCertificateChainErrors:

                    try
                    {
                        if (chain.ChainStatus[0].Status == System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.UntrustedRoot)
                        {
                            // todo: check if remote root certificate is same as local root certificate?
                            // this did not happen without the host entry in the remote certificate?
                            if (fStrictCertificateCheck)
                            {
                                Debug.WriteLine(">> Server's certificate validation has untrusted root -> failed");
                                return false;
                            }
                            else
                            {
                                Debug.WriteLine(">> Server's certificate validation has untrusted root -> pass");
                                Debug.WriteLine(">> Add root certificate to trusted certificates?");
                                return true;
                            }
                        }
                        else
                        {
                            Debug.WriteLine("## Server's certificate validation failed");
                            return false;
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine("## Server's certificate validation failed with exception " + e.Message);
                        return false;
                    }
            }
            Debug.WriteLine("Server's authentication succeeded ...\n");
            return true;
        }

        protected X509Certificate SelectClientCertificate(object sender, string targetHost, X509CertificateCollection localCertificates, X509Certificate remoteCertificate, string[] acceptableIssuers)
        {
            return localCertificates[0]; // return the first certificate
        }

        protected override bool SetupConnection()
        {
            try
            {
                fClient = new TcpClient(fRemoteHost, fRemotePort);
                fClient.LingerState.LingerTime = 1;
                fClient.LingerState.Enabled = true;
            }
            catch
            { } //  catch and ignore all exceptions, just do not connect
            if (fClient != null)
            {
                // TLS part
                fDataStream = new SslStream(
                    fClient.GetStream(),
                    false,
                    new RemoteCertificateValidationCallback(CheckRemoteCertificate),
                    new LocalCertificateSelectionCallback(SelectClientCertificate),
                    EncryptionPolicy.RequireEncryption);
                try
                {
                    fDataStream.AuthenticateAsClient(fRemoteHost, fCertificates, SslProtocols.Tls12, false); // true);  // todo: checkCertificateRevocation
                    return true;
                }
                catch (Exception e)
                {
                    Debug.WriteLine("## IMB TLS authentication exception " + e.Message);
                    FinishConnection();
                }
            }
            // if we fall through to here we failed setting up the connection
            return false;
        }

        protected override void FinishConnection()
        {
            fClient.Close();
            fClient = null;
            fDataStream.Close();
            fDataStream = null;
        }

        protected override int ReadBytes(byte[] aBuffer, int aOffset, int aLimit)
        {
            if (Connected)
            {
                int totBytesReceived = 0;
                int bytesReceived;
                do
                {
                    bytesReceived = fDataStream.Read(aBuffer, aOffset, aLimit - aOffset);
                    aOffset += bytesReceived;
                    totBytesReceived += bytesReceived;
                }
                while (aLimit - aOffset > 0 && bytesReceived > 0);
                return totBytesReceived;
            }
            else
                return -1;
        }

        public override void WritePacket(byte[] aPacket)
        {
            lock (fDataStream)
            {
                fDataStream.Write(aPacket, 0, aPacket.Length);
                if (imbMinimumPacketSize > aPacket.Length)
                {
                    // send filler bytes
                    var fillerBytes = new byte[imbMinimumPacketSize - aPacket.Length];
                    fDataStream.Write(fillerBytes, 0, fillerBytes.Length);
                }
            }
        }
    }
}
